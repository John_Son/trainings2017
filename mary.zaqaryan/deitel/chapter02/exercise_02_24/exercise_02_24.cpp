#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter number: ";
    std::cin >> number;

    if (0 == number % 2) {
        std::cout << "This number is even.\n";
        return 0;
    } 

    std::cout << "This number is odd.\n";
    
    return 0;
}
    
