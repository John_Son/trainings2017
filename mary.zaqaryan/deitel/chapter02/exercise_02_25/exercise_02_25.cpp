#include <iostream>

int
main()
{
    int number1;
    std::cout << "Enter first number: ";
    std::cin >> number1;

    int number2;
    std::cout << "Enter second number: ";
    std::cin >> number2;

    if (0 == number1) {
        std::cout << "Error1: division by zero";
        return 1;
    }
    
    if (number2 % number1 == 0) {
        std::cout << "The first number is a multiple of the second.\n";
        return 0;
    } 
    
    std::cout << "The first number isn't a multiple of the second.\n";
     
    return 0;
}
