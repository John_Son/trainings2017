#include <iostream>

int 
main()
{
    int number1;
    int number2;
    int number3;
    int number4;
    int number5;

    std::cout << "Please Enter 5 numbers: ";
    std::cin >> number1 >> number2 >> number3 >> number4 >> number5;

    int max = number1;
    
    if (max < number2) {
        max = number2;
    };

    if (max < number3) {
        max = number3;
    };

    if (max < number4) {
        max = number4;
    };

    if (max < number5) {
        max = number5;
    };

    int min = number1;

    if (min > number2) {
        min = number2;
    };

    if (min > number3) {
        min = number3;
    };

    if (min > number4) {
        min = number4;
    };

    if (min > number5) {
        min = number5;
    };

    std::cout << "Largest is: " << max << "\n";
    std::cout << "Smallest is: " << min << "\n";

    return 0;
}
