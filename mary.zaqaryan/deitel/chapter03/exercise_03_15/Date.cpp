#include "Date.hpp"
#include <iostream>

Date::Date(int day, int month, int year)
{
    setDay(day);
    setMonth(month);
    setYear(year);
}

void
Date::setDay(int day)
{
    day_ = day;
}

int
Date::getDay()
{
    return day_;
}

void 
Date::setMonth(int month)
{
    if (month > 12) {
        month_= 1;
        return;
    }

    if (month < 1) {
        month_ = 1;
        return;
    }

    month_ = month;
}

int 
Date::getMonth()
{
    return month_;
}

void 
Date::setYear(int year)
{
    year_ = year;
}

int 
Date::getYear()
{       
    return year_;
}

void 
Date::displayDate()
{
    std::cout << getDay() << "/" << getMonth() << "/" << getYear() << "\n";
}

