#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter nonnegative integer number to compute it's factorial." << std::endl;
    std::cin >> number;
    
    if (number < 0) {
        std::cerr << "Error 1: Number can't be negative.";
        return 0;
    }

    int factorial = 1;

    while (number > 1) {
        factorial *= number;
        --number;
    }

    std::cout << "Factorial is: " << factorial;
    return 0;
}
