#include <iostream>

int
main()
{
    int accurancy;
    std::cout << "Enter a accurancy for computing number e." << std::endl;
    std::cin >> accurancy;
    
    double factorial = 1;
    double eNumber = 1;
    int counter = 1;
    
    while (accurancy >= counter) {
        factorial *= counter;
        eNumber += 1 / factorial;
        ++counter;
    }

    std::cout << "e number is: " << eNumber << std::endl;
    return 0;
}
