a:  input x = 5 y = 8
    output  @@@@@
            $$$$$
            &&&&&
    code:
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
        }       
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }

b:  input x = 5 y = 8
    output  @@@@@

    code:
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
            std::cout << "$$$$$" << std::endl;
            std::cout << "&&&&&" << std::endl;
        }
    }

c:  input x = 5 y = 8
    output  @@@@@
            &&&&&

    code:
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        } else {
            std::cout << "#####" << std::endl;
            std::cout << "$$$$$" << std::endl;
        }   
        std::cout << "&&&&&" << std::endl;
    }

d:  input x = 5 y = 7
    output  #####
            $$$$$
            &&&&&
            
    code:
    if (8 == y) {
        if (5 == x) {
            std::cout << "@@@@@" << std::endl;
        }
    } else {
        std::cout << "#####" << std::endl;
        std::cout << "$$$$$" << std::endl;
        std::cout << "&&&&&" << std::endl;
    }

