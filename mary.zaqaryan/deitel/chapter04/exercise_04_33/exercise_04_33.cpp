#include <iostream>

int 
main()
{  
    int side1;
    int side2;
    int side3;

    std::cout << "Enter 3 nonzero numbers. ";
    std::cin >> side1 >> side2 >> side3;
     
    if (side1 <= 0) {
        std::cerr << "Error 1: Side can't be zero or nedative.";
        return 1;
    }
    if (side2 <= 0) {
        std::cerr << "Error 1: Side can't be zero or nedative.";
        return 1;
    }
    if (side3 <= 0) {
        std::cerr << "Error 1: Side can't be zero or nedative.";
        return 1;
    }

    int doubleSide1 = side1 * side1;
    int doubleSide2 = side2 * side2;
    int doubleSide3 = side3 * side3;
    if (doubleSide1 == doubleSide2 + doubleSide3) {
        std::cout << "The right triangle can be formed." << std::endl;
        return 0;
    }
    if (doubleSide2 == doubleSide1 + doubleSide3) {
        std::cout << "The right triangle can be formed." << std::endl;
        return 0;
    } 
    if (doubleSide3 == doubleSide1 + doubleSide2) {
        std::cout << "The right triangle can be formed." << std::endl;
        return 0;
    }

    std::cout << "The right triangle can't be formed" << std::endl;
    
    return 0; 
}
