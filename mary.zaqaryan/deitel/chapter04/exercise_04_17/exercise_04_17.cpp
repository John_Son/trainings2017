#include <iostream>
#include <climits>

int 
main()
{
    int counter = 1;

    std::cout << "Enter 10 numbers\n";
    int largest = INT_MIN; 

    while (counter <= 10) {
        int number;
        std::cin >> number;

        if (number > largest) {
            largest = number;
        }
        ++counter;
    }
    std::cout << "The largest is: " << largest << std::endl;
    return 0;
}
