#include <iostream>

int 
main()
{
    int number;
    std::cout << "Enter a number to decode it: ";
    std::cin >> number;

    if (number < 1000) {
        std::cerr << "Error1: Enter only 4 digit number." << std::endl;
        return 1;
    }
    if (number > 9999) {
        std::cerr << "Error1: Enter only 4 digit number." << std::endl;
        return 1;
    }

    int digit1 = (number / 1000 + 3) % 10;
    int digit2 = (number / 100 + 3) % 10;
    int digit3 = (number / 10 + 3) % 10;
    int digit4 = (number + 3) % 10;

    int decodedNumber = digit3 * 1000 + digit4 * 100 + digit1 * 10 + digit2;

    std::cout << "Decoded number: " << decodedNumber << std::endl;
    return 0;
}
