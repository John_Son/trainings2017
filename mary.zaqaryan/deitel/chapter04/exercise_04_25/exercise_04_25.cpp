#include <iostream>

int 
main()
{
    int sideSize;

    std::cout << "Enter the size of the side of the square: ";
    std::cin >> sideSize;
    std::cout << "\n";

    if (sideSize <= 0) {
        std::cerr << "Error 1: Invalid size." << std::endl;
        return 1;
    }

    int counter1 = 1;

    while (counter1 <= sideSize) {
        int counter2 = 1;
        while (counter2 <= sideSize) {
            if (1 == counter1 || sideSize == counter1) {
                std::cout << "*";
            } else if (1 == counter2 || sideSize == counter2) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++counter2;
        }

        std::cout << std::endl;
        ++counter1;
    }
    return 0;
}
