#include "Analysis.hpp"
#include <iostream>

void Analysis::processExamResults()
{
    int passes = 0;
    int failures = 0;
    int studentCounter = 1;

    while (studentCounter <= 10) {
        int result;
        std::cout << "Enter result (1 = pass, 2 = fail): ";
        std::cin >> result;
        std::cout << "\n";
        

        if (1 == result) {
            ++passes;
            ++studentCounter;
        } else if (2 == result) {
            ++failures;
            ++studentCounter;
        } else {
            std::cout << "Warning 1: Please enter a correct value" << std::endl;
        }
    }

    std::cout << "Passes " << passes << "\nFailed" << failures << std::endl;

    if (passes > 8) {
        std::cout << "Raise tuition " << std::endl;
    }
}
