#include <iostream>

int 
main()
{
    std::cout << "Year " << "Deposit amount\n";

    for (int rate = 5; rate <= 10; ++rate) {
        std::cout << rate << "% Rate:\n";
        
        double principal = 1000.0;
        double rateIncrement = 1 + rate / 100.0;

        for (int year = 1; year <= 10; ++year) {
            principal *= rateIncrement;
            std::cout << year << " " << principal << "\n";
        }
        std::cout << std::endl;
    }
}
