#include <iostream>

int 
main()
{
    double sum = 0;
    for (int i = 0; i < 5; ++i) {
        int productNumber;
        std::cin >> productNumber;
        
        int productAmount;
        std::cin >> productAmount;

        if (productAmount < 0) {
            std::cerr << "Error 1: Amount can't be negative.";
            return 1;
        }

        switch (productNumber) {
            case 1: sum += productAmount * 2.98; break;
            case 2: sum += productAmount * 4.50; break;
            case 3: sum += productAmount * 9.98; break;
            case 4: sum += productAmount * 4.49; break;
            case 5: sum += productAmount * 6.87; break;
            default: std::cerr << "Error 2: Invalid product number"; return 2;
        }
    }
    std::cout << "Sum: " << sum << std::endl;

    return 0;
}

