#include "MiniComputer.hpp"

#include <iostream>

int
main()
{
    MiniComputer mini("Mini");
    return mini.run();
}


