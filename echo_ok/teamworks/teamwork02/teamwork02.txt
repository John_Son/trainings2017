Requirements of the program modelling the watch


1. Different watches have different modelings, so in order to provide more detailed information, we decided to take a specific type of a watch - the Digital watch. 
Hence, for the program of modelling the digital watch, first of all, we need to describe the hardware components of the interface the user uses.

	1. The digital watch has four buttons:
		1.1 Reset. When you push this button, it opens up the display where the user can change data: date and time.

		1.2 Mode. This button provides the opportunity to change and use other functions of a watch. It shows two displays: time and stopwatch.

		1.3 Start/Stop. While the watch is modded to stopwatch display, we can start and stop it by pushing this button.

		1.4 Light. The button's only function is to lighten up the display, in order to see the content in the darkness.

	2. "Battery replacing" part is located on the backside of a watch. 
	
2. After declaring these components, we should supply corresponding data and functions.
	1. The time should be in am/pm format ( i.e., 4:00 pm, not 16:00.) and must show the second count up.

	2. The date should be in this format: Weekday, Month day (Friday, Sep 9) 

	3. While resetting the time or date, it should start with the very first data (number or word), and it should start appearing and disappearing alternately.   
           During this period the user can change the content with this alternative buttons:
	    	
		3.1 Arrow up. It increases the values, months and weekdays in specific orders.

		3.2 Arrow down. And this one decreases the same values.
	
	4. The StopWatch should be in in this format: ( 00:00:00 / minute:second:nanosecond )

3. Also we did some research and found out the mechanism of a digital watch.

Battery: 
Digital watches are a popular and important piece of technology that allow you to keep a portable measurer of time around with you. If you wear a digital watch, then you already know how useful it can be. Even better than traditional watches, these display numbers in an easy-to-read manner that even show in the dark. To begin to understand how these watches work, we have to start at the source of their power: the battery. Watch batteries are small, round and offer the power needed to work the watch. They are directly connected to a crystal oscillator.

Crystal Oscillator: 
A crystal oscillator is made out of quartz crystal and uses power from the watch's battery in order to create a 60-hertz signal. Each hertz represents one oscillation per minute, which is a necessary number in order for the watch to accurately display the time.

Counter:
The crystal oscillator sends this signal, which is known as the time base, through a counter next. This is a preprogrammed device that begins to break up the time base by dividing it by certain numbers. First, it is divided by 10, and then again by 6. This sets up the clock to start counting seconds, which will allow it to accurately display what time it is. When the counter is done, a binary number is achieved.

Display:
This binary number is then sent to a special microchip known as a "binary number to 7-segment display converter." Though it doesn’t have a fancy name, it does have one that tells you just what it is going to do. It is programmed to convert the binary number into a series of seven numbers. These all correspond to the hours, minutes and seconds of the current time. This time is then displayed on the face of the watch through LED lights that are controlled to display the numbers the chip comes up with. This is how a watch is able to display the accurate time.

