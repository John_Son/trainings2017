#include <iostream>
#include <iomanip>

int
main()
{
    double salesPerWeek = 0.0;
    while (salesPerWeek != -1) {
        std::cout << "Enter sales in dollars (-1 to end): ";
        std::cin >> salesPerWeek;
        if (-1 == salesPerWeek) {
            return 0;
        }
        if (salesPerWeek < 0) {
            std::cerr << "Error 1: Value of sales cannot be negative." << std::endl;
            return 1;
        }
        double salary = salesPerWeek * 0.09 + 200;
        std::cout << "Salary is: $" << std::setprecision(2) << std::fixed << salary << std::endl; 
    }
    return 0;
}
