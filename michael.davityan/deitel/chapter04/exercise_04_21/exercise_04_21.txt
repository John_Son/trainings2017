Qusetion: What does the following program print?

#include <iostream>
using std::cout;
using std::endl;
int main()
{
    int count = 1; // initialize count
    while ( count <= 10 ) // loop 10 times
    {
        // output line of text
        cout << ( count % 2 ? "****" : "++++++++" ) << endl;
        count++; // increment count
    } // end while
    return 0; // indicate successful termination
} // end main

Answer: The following program print this!!!!!

++++++++
****
++++++++
****
++++++++
****
++++++++
****
++++++++
****
