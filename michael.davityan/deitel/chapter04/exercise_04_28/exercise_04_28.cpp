#include <iostream>

int
main()
{
    int lineQuantity;

    std::cin >> lineQuantity;

    if (lineQuantity < 0) {
        std::cout << "Error 1: wrong number. " << std::endl;
        return 1;
    }

    int count = 1;

    while(count <= lineQuantity) {
        int count1 = 1;

        if (0 == count % 2) {
            std::cout << " ";
        }

        while(count1 <= lineQuantity) {
            std::cout << "* ";
            ++count1;
        }
        ++count;  
        std::cout << std::endl;
    }

    return 0;
}
