#include <string> 

class GradeBook
{
public:
    GradeBook(std::string courseName, std::string teacherName); 
    void setCourseName(std::string courseName); 
    void setTeacherName(std::string teacherName);
    std::string getCourseName();
    std::string getTeacherName(); 
    void displayMessage(); 

private:
    std::string courseName_;
    std::string teacherName_;
};
