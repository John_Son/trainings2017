Generaly when we build the class object, to initialize class member-variables by value we use constructor function 
defined in class. Constructor is class named function that can take values as arguments and set class member 
variables. Constructor dont return anything(have returning type void).

It works like this!!!!

When we build class object we can initialize class member-variables by giving values to object as arguments.
Its automaticaly go to constructor and set class member-variables.

But if we dont have constructor defined in class, when we build object class call default constructor that initialize automaticaly class member-variables by non-defined values. 
