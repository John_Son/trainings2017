#include "Invoice.hpp"
#include <iostream>
#include <string>

int
main()
{
    int partNumber;
    std::string partDescription;
    int itemQuantity;
    int pricePerItem;

    std::cout << "Insert part number: " << "\n";
    std::cin >> partNumber;
    std::cout << "Insert part description: " << "\n";
    std::cin.ignore();
    std::getline(std::cin, partDescription);
    std::cout << "Insert item quantity: " << "\n";
    std::cin >> itemQuantity;
    std::cout << "Insert price per item: " << "\n";
    std::cin >> pricePerItem;

    Invoice partInvoice(partNumber, partDescription, itemQuantity, pricePerItem);
    std::cout << partInvoice.getPartNumber() << "\n";
    std::cout << partInvoice.getPartDescription() << "\n";
    std::cout << partInvoice.getItemQuantity() << "\n";
    std::cout << partInvoice.getPricePerItem() << "\n";
    std::cout << partInvoice.getInvoiceAmount() << std::endl;

    return 0;
}
