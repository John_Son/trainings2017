#include<iostream>

int
main()
{
/// Okay!!!. Now this is the first requirement of the exercise.

    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n\n";

/// And this is a second!!!.

    std::cout << "* * * * * * * *\n"
                 " * * * * * * * *\n"
                 "* * * * * * * *\n"
                 " * * * * * * * *\n"
                 "* * * * * * * *\n"
                 " * * * * * * * *\n"
                 "* * * * * * * *\n"
                 " * * * * * * * *" << std::endl;

    return 0;
}
