#include <iostream>
#include <unistd.h>
#include <cmath>

int
main()
{
    int rhombusSize;
    if (::isatty(STDIN_FILENO)) {
        std::cout << "Insert rhombus size (must be odd number from 1->19): ";
    }
    std::cin >> rhombusSize;
    if (rhombusSize % 2 == 0 || rhombusSize < 1 || rhombusSize > 19) {
        std::cerr << "Error 1: wrong size value (must be odd number from 1->19).\n";
        return 1;
    }
    const int rowLimit = rhombusSize / 2;
    for (int row = -rowLimit; row <= rowLimit; ++row) {
        const int starStart = rowLimit - std::abs(row);
        for (int column = -rowLimit; column <= starStart; ++column) {
            std::cout << (column < -starStart ? " " : "*");
        }
        std::cout << std::endl;
    } 
    return 0;
}
