#include <iostream>

int
main()
{
    /// This values of x and y are for example.
    int x = 1;
    int y = 2;

    /// This is (a) point of exercise... look for output values to see similarity.
    std::cout << (!(x < 5) && !(y >= 7)) << "\n";
    std::cout << !(x < 5 || y >= 7) << "\n";

    /// This values of a, b and g are for example.
    int a = 3;
    int b = 3;
    int g = 5;

    /// This is (b) point of exercise... look for output values to see similarity.
    std::cout << (!(a == b) || !(g != 5)) << "\n";
    std::cout << !((a == b) && (g != 5)) << "\n";

    /// This is (c) point of exercise... look for output values to see similarity.
    std::cout << !((x <= 8) && (y > 4)) << "\n";
    std::cout << (!(x <= 8) || !(y > 4)) << "\n";

    /// This values of i and j are for example.
    int i = 5;
    int j = 9;

    /// This is (d) point of exercise... look for output values to see similarity.
    std::cout << !((i > 4) || (j <= 6)) << "\n";
    std::cout << (!(i > 4) && !(j <= 6)) << std::endl;

    return 0;
}

