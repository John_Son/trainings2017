#include <iostream>
#include <iomanip>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

int
main()
{
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    } 
    const int ARRAY_SIZE = 11;
    int dicesValuesSumm[ARRAY_SIZE] = {0};
    const int THROW_AMOUNT = 36000;
    for (int count = 1; count <= THROW_AMOUNT; ++count) {
        const int dice1 = 1 + rand() % 6;
        const int dice2 = 1 + rand() % 6;
        const int sum = dice1 + dice2;
        ++dicesValuesSumm[sum - 2]; 
    }

    std::cout << "sum\tfrequency\n";
    for (int row = 0; row < ARRAY_SIZE; ++row) {
        std::cout << row + 2 << "\t" << dicesValuesSumm[row] << std::endl;
    }

    return 0;    
}
