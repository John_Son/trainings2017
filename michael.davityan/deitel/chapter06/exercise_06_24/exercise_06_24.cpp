#include <iostream>
#include <unistd.h>
#include <cassert>
#include <cmath>

void selectAndPrint(const int figureCode, const int sideLength, const char drawingCharacter);
void filledRectangle(const int sideLength, const char drawingCharacter);
void emptyRectangle(const int sideLength, const char drawingCharacter);
void triangle(const int sideLength, const char drawingCharacter);
void rhombus(const int sideLength, const char drawingCharacter);

int
main() 
{
    if (::isatty(STDIN_FILENO)) {
        std::cout << "...This program takes from user figure code and\n" 
                  << "figure drawing character and print figure listed below....\n"
                  << "1 -> filled rectangle.\n"
                  << "2 -> empty rectangle.\n"
                  << "3 -> triangle.\n"
                  << "4 -> rhombus.\n"
                  << "insert figure code: ";
    }
    int figureCode;
    std::cin >> figureCode;
    if (figureCode < 1 || figureCode > 4) {
        std::cerr << "Error 1: wrong code number(must be from 1 to 4).\n";
        return 1;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "insert figure side length(for rhombus it must be odd.): ";
    }
    int sideLength;
    std::cin >> sideLength;
    if (sideLength < 1) {
        std::cout << "Error 2: wrong length(must be more than zero).\n";
        return 2;
    }
    if ((4 == figureCode) && (0 == sideLength % 2)) {
        std::cout << "Error 3: wrong length for rhombus(must be odd number).\n";
        return 3;
    }
    if (::isatty(STDIN_FILENO)) {
        std::cout << "insert drawing character: ";
    }
    char drawingCharacter;
    std::cin >> drawingCharacter;
    selectAndPrint(figureCode, sideLength, drawingCharacter);
    return 0;
}

void 
selectAndPrint(const int figureCode, const int sideLength, const char drawingCharacter) 
{
    assert(figureCode >= 1 && figureCode <= 4);
    switch (figureCode) 
    {
        case 1: filledRectangle(sideLength, drawingCharacter) ; break;
        case 2: emptyRectangle(sideLength, drawingCharacter)  ; break;
        case 3: triangle(sideLength, drawingCharacter)        ; break;
        case 4: rhombus(sideLength, drawingCharacter)         ; break;
    }
}

void 
filledRectangle(const int sideLength, const char drawingCharacter)
{
    assert(sideLength > 0);
    for (int row = 1; row <= sideLength; ++row) {
        for (int column = 1; column <= sideLength; ++column) {
            std::cout << drawingCharacter;
        }
        std::cout << std::endl;
    }
}

void 
emptyRectangle(const int sideLength, const char drawingCharacter) 
{
    assert(sideLength > 0);
    for (int row = 1; row <= sideLength; ++row) {
        for (int column = 1; column <= sideLength; ++column) {
            if (row == 1 || row == sideLength || column == 1 || column == sideLength) {
                std::cout << drawingCharacter;
            } else {
                std::cout << " ";
            }
        }
        std::cout << std::endl;    
    }
}

void 
triangle(const int sideLength, const char drawingCharacter)
{
    assert(sideLength > 0);
    for (int row = 1; row <= sideLength; ++row) {
        for (int column = 1; column <= row; ++column) {
            std::cout << drawingCharacter;
        }
        std::cout << std::endl;
    }
}

void 
rhombus(const int sideLength, const char drawingCharacter) {
    assert((sideLength > 0) && (sideLength % 2 != 0));
    const int lineLimit = (sideLength - 1) / 2;
    for (int row = -lineLimit; row <= lineLimit; ++row) {
        const int probelLimit = lineLimit - std::abs(row);
        for (int column = -lineLimit; column <= probelLimit; ++column) {
            if (column < -probelLimit) {
                std::cout << " ";
            } else {
                std::cout << drawingCharacter;
            }
        }
        std::cout << std::endl;
    }
}
