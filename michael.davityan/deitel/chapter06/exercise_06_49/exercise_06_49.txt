Question!!!!.
------------------------------------------------------------------------------------------------------
What is wrong with the following program?
 #include <iostream>
 using std::cin;
 using std::cout;

 int main()
 {
    int c;

    if ((c = cin.get()) != EOF)
    {
        main();
        cout << c;
    } 

    return 0;
 }

Answer!!!!.
------------------------------------------------------------------------------------------------------
1. cin.get() takes only characters, not integer values.
2. How about cout << c line in all main() recursive steps this line work until we press ctrl+d.
But they are stay in background because of first main() recursive call. After pressing ctrl+d
all recursive called main() cout-s shown on display.......

I think this code much better!!!!!.
#include <iostream>
 using std::cin;
 using std::cout;

 int 
 main()
 {
    char c;

    if (( c = cin.get()) != EOF) {
        main();
        cout << c;
    }

    return 0;
 }

