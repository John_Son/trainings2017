Question. Write statements that assign random integers to the variable n in the following ranges:
a) 1 <= n <= 2
b)1 <= n <= 100
c) 0 <= n <= 9
d) 1000 <= n <= 1112
e) ֊1 <= n <= 1
f) -3 <= n <= 11

Answer.
a)n = 1 + rand() % 2;
b)n = 1 + rand() % 100;
c)n = rand() % 10;
d)n = 1000 + rand() % 113;
e)n = rand() % 3 - 1;
f)n = rand() % 15 - 3;
