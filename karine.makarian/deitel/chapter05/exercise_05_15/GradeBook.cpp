#include <iostream>
#include "GradeBook.hpp"

GradeBook::GradeBook(std::string name)
{
    setCourseName(name);
    aCount_ = 0;
    bCount_ = 0;
    cCount_ = 0;
    dCount_ = 0;
    fCount_ = 0;
}

void
GradeBook::setCourseName(std::string name)
{
    if (name.length() <= 25) {
        courseName_ = name;
    } else {
        courseName_ = name.substr(0, 25);
        std::cout << "Name \"" 
            << name
            << "\" exceeds maximum length (25)."
            << std::endl
            << "Limiting courseName to first 25 characters."
            << std::endl;
    }
}

std::string
GradeBook::getCourseName()
{
    return courseName_;
}

void
GradeBook::displayMessage()
{
    std::cout << "Welcome to the grade book for"
        << std::endl
        << getCourseName()
        << "!" 
        << std::endl;
}

void
GradeBook::inputGrades()
{
    int grade;
    std::cout << "Enter the letter grades (q to quit)." << std::endl;

    while ((grade = std::cin.get()) != 'q') {
        switch (grade) {
        case 'A': case 'a': ++aCount_; break;
        case 'B': case 'b': ++bCount_; break;
        case 'C': case 'c': ++cCount_; break;
        case 'D': case 'd': ++dCount_; break;
        case 'F': case 'f': ++fCount_; break;
        default:
            std::cerr << "Error 1: Incorrect letter grade." << std::endl;
            return 1;
        }
    } 
} 

void
GradeBook::displayGradeReport()
{
    std::cout << std::endl 
        << "Number of students who received each letter grade:"
        << std::endl 
        << "A: " << aCount_
        << std::endl 
        << "B: " << bCount_
        << std::endl 
        << "C: " << cCount_
        << std::endl 
        << "D: " << dCount_
        << std::endl 
        << "F: " << fCount_
        << std::endl;
}

void
GradeBook::displayAverage()
{
    const double quantity = aCount_ + bCount_ + cCount_ + dCount_ + fCount_;
    double average = (aCount_ * 4.0 + bCount_ * 3.0 + cCount_ * 2.0 + dCount_ * 1.0) / quantity; 

    std::cout << std::endl << "The Average of grades: " << average << std::endl;
}

