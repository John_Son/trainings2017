

///a.
const int SIZE = 5;
unsigned int values[SIZE];  ///or values[SIZE] = {2,4,6,8,10};
int j = 2;
for (int i = 0; i < SIZE; ++i) {
    values[i] = j;
    j += 2; 
}

///b.
unsigned int x = 5;
unsigned int *vptr = &x;

///c.
for (int i = 0; i < SIZE; ++i) {
    std::cout << values[i] << ' ';
}

///d.
vptr = values;
vptr = &values[0];

///e.
for (int i = 0; i < SIZE; ++i) {
    std::cout << *(vptr + i) << ' ';
}

///f.
for (int i = 0; i < SIZE; ++i) {
    std::cout << *(values + i) << ' ';
}

///g.
unsigned int *ptr = values;
for (int i = 0; i < SIZE; ++i) {
    std::cout << ptr[i] << ' ';
}

///h.
unsigned int * tPtr = *(vptr + 4);
unsigned int * tPtr = *(values + 4);
unsigned int * tPtr = vptr[4];
unsigned int * tPtr = values[4];
///i.
The address is 1002500 + 2 * 3 = 1002506, and the value = 8

///j.
The value of vptr[0], which is 2,and its location is 1002500
