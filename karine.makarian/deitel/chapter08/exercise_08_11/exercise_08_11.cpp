///a.
void zero(long int bigIntegers[]) {};

///b.
void zero(long int *);
    
///c.
int add1AndSum(int oneTwoSmall[]) {};

///d.
int add1AndSum(int *);
