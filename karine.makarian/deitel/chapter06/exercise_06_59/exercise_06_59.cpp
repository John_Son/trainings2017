#include <iostream>

template < typename K > 
K
maximum( const K value1, const K value2, const K value3 )
{
    K maxValue = value1; 
    if ( value2 > maxValue ) {
        maxValue = value2;
    }
    
    if ( value3 > maxValue ) {
        maxValue = value3;
    }
    
    return maxValue;
}

int 
main()
{
    int int1, int2, int3;
    std::cout << "Input three integer values: ";
    std::cin >> int1 >> int2 >> int3;
 
    std::cout << "The maximum integer value is: "
              << maximum( int1, int2, int3 ) << std::endl;
        
    double double1, double2, double3;
    std::cout << "Input three double values: ";
    std::cin >> double1 >> double2 >> double3;
    
    std::cout << "The maximum double value is: "
              << maximum( double1, double2, double3 ) << std::endl;
             
    char char1, char2, char3;
    std::cout << "Input three characters: ";
    std::cin >> char1 >> char2 >> char3;
         
    std::cout << "The maximum character value is: "
             << maximum( char1, char2, char3 ) << std::endl;
    
    return 0;
}
