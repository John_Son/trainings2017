#include <iostream>
#include <cassert>

int
time(const int hours, const int minutes, const int seconds) 
{
    assert(hours >= 0 && minutes >= 0 && seconds >= 0);
    return hours * 3600 + minutes * 60 + seconds;
}

int
main()
{
    const int passedSeconds1 = time(1, 30, 30);
    const int passedSeconds2 = time(12, 3, 10);

    std::cout << "First moment's passed seconds: " << passedSeconds1 << std::endl
        << "Second moment's passed seconds: " << passedSeconds2 << std::endl
        << "Interval in seconds: " << passedSeconds2 - passedSeconds1 << std::endl;

    return 0;
}

