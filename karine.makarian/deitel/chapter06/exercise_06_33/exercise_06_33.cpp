#include <iostream>
#include <cassert>

int
qualityPoints(const int point) 
{
    assert(point >= 0);
    return (point - 50) > 0 ? (point - 50) / 10 : 0;
}

int
main()
{
    int average;
    std::cout << "Enter student's average mark ";
    std::cin >> average;
    if (average < 0) {
        std::cerr << "Error 1: Average can't be negative." << std::endl;
        return 1;
    }

    std::cout << "Result for this average is " << qualityPoints(average) << std::endl;

    return 0;
}
