#include <iostream>
#include <cstdlib>
#include <ctime>
#include <unistd.h>

void
responseForGuessCount(const int guessCount)
{
    if (guessCount < 10) {
        std::cout << "Either you know the secret or you got lucky !" << std::endl;
    } else if (10 == guessCount) {
        std::cout << "A hah! You know the secret!" << std::endl;
    } else {
        std::cout << "You should be able to do better!" << std::endl;
    }
}

int
main()
{    
    if (::isatty(STDIN_FILENO)) {
        std::srand(std::time(0));
    }

    const int numberToBeGuessed = 1 + std::rand() % 1000;
    int guessCount = 0;
 
    while (true) {
        if (0 == guessCount) {
            std::cout << "I have a number between 1 and 1000.\n"
                      << "Can you guess my number?\n"
                      << "Please type your first guess." << std::endl;
        }

        int answer;
        std::cin >> answer;

        ++guessCount;
        if (answer < numberToBeGuessed) {
            std::cout << "Too low.Try again." << std::endl;
        } else if (answer > numberToBeGuessed) {
            std::cout << "Too high.Try again." << std::endl;
        } else {
            std::cout << "Excellent! You guessed the number!\n";
            responseForGuessCount(guessCount);
            std::cout << "Would you like to play again (y or n)?" << std::endl;
 
            char answerToQuit;
            std::cin >> answerToQuit;
                
            if ('y' == answerToQuit || 'Y' == answerToQuit) {
                guessCount = 0;
            } else if ('n' == answerToQuit || 'N' == answerToQuit) {
                break;
            }       
        }   
    } 
     
    return 0;
}

