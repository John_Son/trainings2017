#include <iostream>
#include <cassert>

void
printSquare(int size1, char character)
{
    assert(size1 > 0);
    for (int i = 1; i <= size1; ++i) {
        for (int j = 1; j <= size1; ++j) {
            std::cout << character;
        }

        std::cout << std::endl;
    }
}

int
main()
{
    int size;
    std::cout << "Enter the size of a square: ";
    std::cin >> size;

    if (size <= 0) {
        std::cerr << "Error 1: Non positive size." << std::endl;
        return 1;
    }
    char character;
    std::cout << "Enter a character: ";
    std::cin >> character;

    printSquare(size, character);
    return 0;
}
