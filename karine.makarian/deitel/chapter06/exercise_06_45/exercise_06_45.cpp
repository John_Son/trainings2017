#include <iostream>

int gcd(const int x, const int y);

int
main()
{
    int num1, num2;
    std::cout << "Enter 2 integers to find gcd. ";
    std::cin >> num1 >> num2;

    const int result = gcd(num1, num2);
    std::cout << "Greatest common divosor of entered integers is " << result << std::endl;

    return 0;
}

int
gcd(const int x, const int y)
{
    if (0 == y) {
        return x;        
    }
    
    return gcd(y, x % y);
}
