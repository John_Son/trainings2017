#include <iostream>

int
main()
{
    int number1;
    int number2;
    std::cout << "Enter first number: ";
    std::cin >> number1;

    std::cout << "Enter second number: ";
    std::cin >> number2;

    if (0 == number1) {
        std::cout << "Error 1: Division by 0\n";
        return 1;
    }

    if (number2 % number1 == 0) {
       std::cout << "The first is a multiple of the second.\n ";
       return 0;
    }

    std::cout << "The first is not a multiple of the second.\n ";
    return 0;
}

