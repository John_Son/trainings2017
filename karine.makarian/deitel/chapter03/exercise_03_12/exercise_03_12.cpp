#include "Account.hpp"
#include <iostream>

int
main()
{
    Account account1(1500);
    Account account2(-500);
    int creditAmount = 4500;
    int debitAmount = 800;   

    std::cout << "Account 1's balance is " << account1.getBalance() << std::endl;
    std::cout << "Account 2's balance after credit is " << account2.credit(creditAmount) << std::endl;
    std::cout << "Account 1's balance after debit is " << account1.debit(debitAmount) << std::endl;
    debitAmount = 1000;
    std::cout << "Account 1's balance after 2nd debit is " << account1.debit(debitAmount) << std::endl;
    creditAmount = 8600;
    account2.setBalance(creditAmount);
    std::cout << "Account 2's balance after setting is " << account2.getBalance() << std::endl;
   
    return 0;
}

