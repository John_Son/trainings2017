#include "Invoice.hpp"
#include <iostream>
#include <string>

Invoice::Invoice(std::string number, std::string description, int quantity, int pricePerItem)
{
    setNumber(number);
    setDescription(description);
    setQuantity(quantity);
    setPricePerItem(pricePerItem);
}

void
Invoice::setNumber(std::string number)
{
    number_ = number;
}

std::string 
Invoice::getNumber()
{
    return number_;
}

void
Invoice::setDescription(std::string description)
{
    description_ = description;
}

std::string 
Invoice::getDescription()
{
    return description_;
}

void 
Invoice::setQuantity(int quantity)
{
    if (quantity < 0) {
        quantity_ = 0;
        std::cout << "Error 1: Non positive quantity was set. It is setted to 0." << std::endl;
        return;
    }
    quantity_ = quantity;
}

int 
Invoice::getQuantity()
{
    return quantity_;
}

void
Invoice::setPricePerItem(int pricePerItem)
{
    if (pricePerItem < 0) {
        pricePerItem_ = 0;
        std::cout << "Error 2 : Non positive price was set. It is setted to 0." << std::endl;
        return;
    }
    pricePerItem_ = pricePerItem;
}

int
Invoice::getPricePerItem()
{
    return pricePerItem_;
}

int
Invoice::getInvoiceAmount()
{
    return quantity_ * pricePerItem_;
}

