#include <iostream>

int
main()
{
    while (true) {
        int accountNumber;

        std::cout << "Enter your account number (-1 to exit): ";
        std::cin >> accountNumber;

        if (-1 == accountNumber) {
            break;
        }
    
        double initialBalance;

        std::cout << "Enter the initial balance: ";
        std::cin >> initialBalance;

    
        if (initialBalance < 0) {
            std::cerr << "Error 1: The initial balance can't be negative." << std::endl;
            return 1;
        }

        double totalCharges;

        std::cout << "Enter the total charges: ";
        std::cin >> totalCharges;

        if (totalCharges < 0) {
            std::cerr << "Error 2: The total charges can't be negative." << std::endl;
            return 2;
        }

        double totalCredits;

        std::cout << "Enter the total credits: ";
        std::cin >> totalCredits;

        if (totalCredits < 0) {
            std::cerr << "Error 3: The total credits can't be negative." << std::endl;
            return 3;
        }

        double creditLimit;

        std::cout << "Enter limit of credit: ";
        std::cin >> creditLimit;

        if (creditLimit < 0) {
            std::cerr << "Error 4: The limit of credit can't be negative." << std::endl;
            return 4;
        }

        double newBalance = initialBalance + totalCharges - totalCredits;

        std::cout << "New balance: " << newBalance << std::endl;

        if (newBalance > creditLimit) {
            std::cout << "Account: " << accountNumber << std::endl;
            std::cout << "Credit limit: " << creditLimit << std::endl;
            std::cout << "Balance: " << newBalance << std::endl;
            std::cout << "Credit Limit Exceeded." << std::endl;
        }
    }
            
    return 0;
}

