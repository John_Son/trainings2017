#include <iostream>
#include "Analysis.hpp"

void
Analysis::processExamResults()
{
    int passes = 0;
    int failures = 0;
    int studentCounter = 1;

    while (studentCounter <= 10) {
        int result;

        std::cout << "Enter the result " << studentCounter << " of 10 (1 = pass, 2 = fail): ";
        std::cin >> result;

        ++studentCounter;

        if (1 == result) {
            ++passes;
        } else if (2 == result) {
            ++failures;
        } 
        
        else {
            std::cout << "Enter a correct value." << std::endl;


            --studentCounter;
        }
    }

    std::cout << "Passed " << passes << std::endl;
    std::cout << "Failed " << failures << std::endl;

    if (passes > 8) {
        std::cout << "Raise tuition." << std::endl;
    }
}

