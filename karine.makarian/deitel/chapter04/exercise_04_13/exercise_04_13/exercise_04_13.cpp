#include <iostream>

int
main()
{
    float totalDistance = 0;
    float totalFuel = 0;

    while (true) {
        float miles;
        float gallons;

        std::cout << "Enter passed miles (-1 to exit): ";
        std::cin  >> miles;

        if (-1 == miles) {
            break;
        }

        if (miles < 0) {
            std::cerr << "Error 1: Miles cannot be negative" << std::endl;
            return 1;
        }

        std::cout << "Enter gallons: ";
        std::cin  >> gallons;
        if (gallons <= 0) {
            std::cerr << "Error 2: Gallons cannot be negative" << std::endl;
            return 2;
        }

        std::cout << "MPG this tankful: " << miles / gallons << std::endl;

        totalDistance += miles;
        totalFuel += gallons;

        std::cout << "Total miles/gallon: " << totalDistance / totalFuel << std::endl;
    }

    return 0;
}

