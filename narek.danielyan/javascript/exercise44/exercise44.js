$(document).ready(function () {
    "use strict";
    var firstName = ["The Evil", "The Vile", "The Cruel", "The Trashy", "The Despicable", "The Embarrassing", "The Disreputable", "The Atrocious", "The Twirling", "The Orange", "The Terrifying", "The Awkward"], lastName = ["Mustache", "Pickle", "Hood Ornament", "Raisin", "Recycling Bin", "Potato", "Tomato", "House Cat", "Teaspoon", "Laundry Basket"];
    function getVillianName(date) {
        var month = date.getMonth(), day = date.getDate().toString();
        if (day.length > 0) {
            day = day.slice(-1);
        }
        return firstName[month] + " " +  lastName[+day];
    }
    console.log(getVillianName(new Date(2012, 0, 5)));
});	
