$(document).ready(function () {
    "use strict";
    var arr = [0, 5, 1, 8, 3, 2, 7, 6, 4];
    function getMissingElement(arr) {
        var sum = 0, sumAll = 0, missingDigit = 0;
        for (var i = 0; i < arr.length; ++i) {
            sum += arr[i];
            sumAll += (i + 1);
        }
        missingDigit = sumAll - sum;
        return missingDigit;
    }
    console.log(getMissingElement(arr));
});
