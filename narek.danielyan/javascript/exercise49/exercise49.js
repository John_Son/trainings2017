$(document).ready(function () {
    "use strict";
    var arr = [5, 2, 6, -10, -2, 8, 15];
    function reverseSort(arr) {
        var current = 0;
        for (var i = 0; i < arr.length; ++i) {
            for (var j = i + 1; j < arr.length; ++j) {
                if (arr[j] > arr[i]) {
                    current = arr[i];
                    arr[i] = arr[j];
                    arr[j] = current;
                }   
            }    
        }
        return arr;
    }
    console.log(reverseSort(arr));
});
