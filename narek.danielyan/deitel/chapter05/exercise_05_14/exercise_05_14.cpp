#include<iostream>
#include<iomanip>

int
main()
{  
    float sold; 
    while (true) {
        int product;
        std::cout << "Enter the number of product: ";
        std::cin >> product;
        if (-1 == product) {
            break;
        } 
        int amount;
        std::cout << "\nEnter the amount: ";
        std::cin >> amount;
        switch (product) {
        case 1: sold += amount * 2.98; break;
        case 2: sold += amount * 4.50; break;
        case 3: sold += amount * 9.98; break;
        case 4: sold += amount * 4.49; break;
        case 5: sold += amount * 6.87; break;
        default:
            std::cout << "Error 1: The inputting number should be between 1 and 5" << std::endl;
            return 1;
       }
    }
    std::cout << "\nTotal retail price of all products is " << std::setprecision(2) << std::fixed <<  7 * sold << std::endl;
    return 0;
}    
