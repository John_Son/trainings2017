#include <iostream>

int
main()
{
    int counter = 1;
    float totalSalary = 0;

    while (true) {
        int code;
        std::cout << "Enter code (1-4 or -1 to exit): ";
        std::cin >> code;

        if (-1 == code) {
            break;
        }
        
        float salary = 0;

        switch(code) {
        case 1:
            std::cout << "Enter salary: ";
            std::cin >> salary;
            break;
        case 2:
            std::cout << "Enter hourly salary: ";                
            int hourlySalary; 
            std::cin >> hourlySalary;
            std::cout << "Enter hours worked: ";        
            int hours;    
            std::cin >> hours;    
            salary = (1.5 * hours - 20) * hourlySalary;
            if (hours <= 40) {
                salary = hours * hourlySalary;
            }
            break;
        case 3:
            std::cout << "Enter weekly sales amount: ";            
            int weeklySales;  
            std::cin >> weeklySales;    
            salary = 250 + 0.057 * weeklySales;
            break;
        case 4:
            std::cout << "Enter cost per item: ";
            int cost;
            std::cin >> cost;
            std::cout << "Enter amount of items sold: ";       
            int amount;    
            std::cin >> amount;
            salary = cost * amount;    
            break;
        default:
            std::cerr << "Error 1: Invalid code" << std::endl;    
            return 1;
        }
        totalSalary += salary;
        ++counter;
    }
    std::cout << "Total salary: " << totalSalary << std::endl;
    
    return 0;
}
