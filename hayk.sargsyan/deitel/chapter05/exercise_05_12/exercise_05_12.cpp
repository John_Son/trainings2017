#include <iostream>
#include <iomanip>

int
main()
{
    for (int y = 1; y <= 10; ++y) {
        for (int x = 1; x <= y; ++x) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }

    std::cout << std::endl;

    for (int y = 1; y <= 10; ++y) {
        for (int x = 10; x >= y; --x) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }

    std::cout << std::endl;

    for (int y = 10; y >= 1; --y) {
        std::cout << std::setw(11 - y);
        
        for (int x = 1; x <= y; ++x) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }
    
    std::cout << std::endl;
    
    for (int y = 1; y <= 10; ++y) {
        
        std::cout << std::setw(11 - y);
        
        for (int x = 1; x <= y; ++x) {
            std::cout << "*";
        }

        std::cout << std::endl;
    }

    int counter = 10;

    for (int y = 1; y <= 10; ++y) {
        for (int x = 1; x <= y; ++x) {
            std::cout << "*";
        } 
        for (int x = counter; x >= 1; --x) {
            std::cout << " ";
        }
        
        std::cout << "\t";
        
        for (int x = counter; x >= 1; --x) {
            std::cout << "*";
        }
        for (int x = 1; x <= y; ++x) {
            std::cout << " ";
        } 
        
        std::cout << "\t";

        for (int x = 1; x <= y; ++x) {
            std::cout << " ";
        } 
        for (int x = counter; x >= 1; --x) {
            std::cout << "*";
        }

        std::cout << "\t";

        for (int x = counter; x >= 1; --x) {
            std::cout << " ";
        }
        for (int x = 1; x <= y; ++x) {
            std::cout << "*";
        }

        std::cout << std::endl; 
        
        --counter;
    }
    return 0;
}
