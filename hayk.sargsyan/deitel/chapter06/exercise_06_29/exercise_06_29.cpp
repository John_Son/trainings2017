#include <iostream>

bool 
perfect(const int number)
{
    int sum = 0;
    for (int counter = 1; counter <= number / 2; ++counter) {
        if (0 == number % counter){
            sum += counter;
        }
    }
    return number == sum;
}

int
main()
{
    for (int counter = 1; counter <= 1000; ++counter) {
        if (perfect(counter)) {
            std::cout << counter << " is perfect" << std::endl; 
        }
    }

    return 0;
}
