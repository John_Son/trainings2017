#include <iostream>
#include <cmath>
#include <algorithm>

int 
gcd(int number1, int number2)
{
    while (0 != number2) {
        const int residual = number1 % number2;
        number1 = number2;
        number2 = residual;
    }

    return number1;
}

int
main()
{
    std::cout << gcd(-15, -5) << std::endl;

    return 0;
}
