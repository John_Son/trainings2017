#include <iostream>
#include "Account.hpp"

Account::Account(int balance)
{
    setBalance(balance);
}

void
Account::setBalance(int balance)
{
    if (balance < 0) {
        balance_ = 0;
        std::cout << "Info 1: We are setting your balance to 0, because it can't be less than 0. " << std::endl;
        return;
    }
    balance_ = balance;
}

int
Account::getBalance()
{
    return balance_;
}

int
Account::credit(int amount)
{
    balance_ += amount;
    return balance_;
}

int
Account::debit(int amount)
{
    if (amount > balance_) {
        std::cout << "Your account balance is " << balance_ << ". You can't debit " << amount << "." << std::endl;
        return balance_;
    }
    balance_ -= amount;
    return balance_;
}
