#include <iostream>

int
main()
{
    int radius;

    std::cout << "Enter the radius of a circle: ";
    std::cin >> radius;
    if (0 > radius) {
        std::cout << "Error 1: The radius is negative\n";
	return 1;
    }
    std::cout << "Diameter of circle is " << 2 * radius << std::endl;
    std::cout << "Circumference of circule is " << 2 * 3.14159 * radius << std::endl;
    std::cout << "Area of circule is " << 3.14159 * radius * radius << std::endl;

    return 0;

}
