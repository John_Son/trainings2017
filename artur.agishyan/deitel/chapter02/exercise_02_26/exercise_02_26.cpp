#include <iostream>

int
main()
{
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * * \n";
    std::cout << " * * * * * * * *\n" << std::endl;
                                        
    std::cout << "* * * * * * * * \n * * * * * * * *\n* * * * * * * * \n * * * * * * * * \n* * * * * * * * \n * * * * * * * *\n* * * * * * * * \n * * * * * * * *\n" << std::endl;

    return 0;
}

