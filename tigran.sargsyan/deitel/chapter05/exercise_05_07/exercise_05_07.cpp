#include <iostream>

int
main()
{
    int number1, number2;
    std::cout << "Enter two integers in the range 1-20: ";
    std::cin >> number1 >> number2;

    for (int row = 1; row <= number2; ++row) {
        for (int column = 1; column <= number1; ++column) {
            std::cout << '@';
        }
        std::cout << std::endl;
    }

    return 0;
}

