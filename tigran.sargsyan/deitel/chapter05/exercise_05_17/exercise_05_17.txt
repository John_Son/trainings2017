a) brackets are needed. prints 1 (cause it is true)
b) brackets are needed. prints 0 (cause it is false)
c) brackets are needed. prints 1
d) brackets are needed. prints 0
e) brackets are needed. prints 1
f) brackets are needed. prints 0
g) brackets aren't needed. prints 0
h) outter brackets aren't needed, inner ones are. prints 1 (cause j - m = 0)
i) there is no variable called r. it won't compile
