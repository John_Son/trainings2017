#include "Analysis.hpp"

#include <iostream>

void
Analysis::processExamResults()
{
    int passes = 0, failures = 0, studentCounter = 1;
    while (studentCounter <= 10) {
        int result;
        std::cout << "Enter result (1 = pass, 2 = fail): ";
        std::cin >> result;

        if (1 == result) {
            ++passes;
        } else if (2 == result) {
            ++failures;
        } else {
            std::cout << "Warning. Entered wrong number. Try Again." << std::endl;
            continue;
        }
        ++studentCounter;
    }

    std::cout << "Passed: " << passes << "\nFailed: " << failures << std::endl;

    if (passes > 8) {
        std::cout << "Raise tuition." << std::endl;
    }

}

