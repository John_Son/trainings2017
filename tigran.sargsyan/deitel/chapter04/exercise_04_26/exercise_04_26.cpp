#include <iostream>

int
main()
{
    int number;
    std::cout << "Enter five-digit number: ";
    std::cin >> number;

    if (number < 10000) {
        std::cerr << "Error 1. Entered number isn't five-digit." << std::endl;
        return 1;
    }
    if (number > 99999) {
        std::cerr << "Error 1. Entered number isn't five digit." << std::endl;
        return 1;
    }

    int digit1 = number / 10000;
    int digit2 = number / 1000 % 10;
    int digit4 = number / 10 % 10;
    int digit5 = number % 10;
    if (digit1 == digit5) {
        if (digit2 == digit4) {
            std::cout << "Entered number is palindrome." << std::endl;
        } else {
            std::cout << "Entered number is not palindrome." << std::endl;
        }
    } else {
        std::cout << "Entered number is not palindrome." << std::endl;
    }
    
    return 0;
}

