Mikayel Ghaziyan
06/11/2017
Exercise 3.6


A constructor is a special type of subroutine function called to create an object. The constructor can take arguments and values but it never returns one. If the constructor is not created by the programmer, then it is created by default. 

If the constructor is created by default, it creates an object without assigning any initial values to it. Every constructor implicitly calls the constructors of any data member objects before the body of the class’s constructorexecutes.



