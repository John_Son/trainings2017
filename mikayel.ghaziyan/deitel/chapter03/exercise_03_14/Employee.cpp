#include "Employee.hpp"
#include <iostream>
#include <string>

Employee::Employee(std::string firstName, std::string lastName, int monthlySalary)
{
    setFirstName(firstName);
    setLastName(lastName);
    setSalary(monthlySalary);
}

void
Employee::setFirstName(std::string firstName)
{
    firstName_ = firstName;
}

void
Employee::setLastName(std::string lastName)
{
    lastName_ = lastName;
}

void 
Employee::setSalary(int monthlySalary)
{   
    if (monthlySalary < 0){
	std::cout << "Info 1: The monthly salary cannot have a negative value" << std::endl;
	salary_ = 0;
	return;	
    }   
    salary_ = monthlySalary; 
}

std::string
Employee::getFirstName()
{
    return  firstName_ ;
}

std::string
Employee::getLastName()
{
    return lastName_;
}

int
Employee::getSalary()
{
    return salary_;
}
