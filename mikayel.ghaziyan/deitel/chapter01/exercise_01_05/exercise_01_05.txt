Mikayel Ghaziyan
22/09/2017

Exercise 1.5

a. Why might you want to write a program in a machine-independent
language instead of a machine-dependent language?

Answer - Because you can run the same program on multiple platforms (Windows, Mac and Linux).

b. Why might a machine-dependent language be more appropriate for writing certain types of programs?

Because - First, because the machine-dependent language was created for a specific machine, it can run very fast and be reliable for it. In addition, the language is not portable to other machines, so it remains accustomed to that specific model.
