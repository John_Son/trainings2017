Mikayel Ghaziyan
23/09/2017


Exercise 1.8

Question - Distinguish between the terms fatal error and nonfatal error. Why might you prefer to experience a fatal error rather than a nonfatal error?

Answer - Fatal errors mostly result from wrong syntax or mixing of any core component of the program such as missing declarations of variables or functions. It causes to stop debugging. Nonfatal errors result from the wrong logic of the program. The program can compile and debug but it would not work as it was expected. For example, a programmer may write a program correctly, without any missing parts or syntax errors but fail to write a correct arithmetic formula. The program in the case will compile and run but it`ll make a wrong calculation. Fatal errors are preferable because the compiling not only finds the errors but will also point to those errors so they would be easier to find and correct. At the same time, nonfatal errors remain undetectable.  
