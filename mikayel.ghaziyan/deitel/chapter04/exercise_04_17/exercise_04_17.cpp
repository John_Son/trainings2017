#include <iostream>
#include <climits>

int
main()
{
    int counter = 1;
    int largest = INT_MIN;

    while (counter != 11) {
        int number;
        std::cout << "Enter a number " << counter << ": ";
        std::cin >> number;
        ++counter;
        if (number > largest) {
            largest = number;
        }	
    }

    std::cout << "The largest number is " << largest << std::endl;
	
    return 0;
}
		  
