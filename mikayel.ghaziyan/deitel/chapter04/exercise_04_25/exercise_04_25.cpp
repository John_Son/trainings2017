#include <iostream>

int
main() 
{
    int length;
    std::cin >> length;

    int i = 1;
    while (length >= i) {	
        int j = 1;
        while (length >= j) {
            if (1 == i) {
                std::cout << "*";
            } else if (length == i) {
                std::cout << "*";
            } else if (1 == j) {
                std::cout << "*";
            } else if (length == j) {
                std::cout << "*";
            } else {
                std::cout << " ";
            }
            ++j;	
        }
        std::cout << std::endl;
        ++i;
    }
    return 0;
}
