/// exercise_04_18
#include <iostream>

int
main()
{
    std::cout << "N" << "\t" << "10 * N" << "\t" << "100 * N" << "\t" << "1000 * N" << "\n";
    std::cout << "\n";

    int n = 1;
    while (n <= 5) {
        std::cout << n << "\t" << 10 * n << "\t" << 100 * n << "\t" << 1000 * n << "\n";
        ++n;
    }

    std::cout << std::endl;
    return 0;
}
		  
