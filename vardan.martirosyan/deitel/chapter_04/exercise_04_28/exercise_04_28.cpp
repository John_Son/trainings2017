#include <iostream>

int
main()
{
    int size;
    std::cout << "Enter size: ";
    std::cin  >> size;
    
    if (size <= 0) {
        std::cerr << "Error 1: Size should be positive." << std::endl;
        return 1;
    }
    int j = 1;
    while (j <= size) {
        if (1 == (j % 2)) {
            std::cout  << " ";
        }
        int i = 1;
        while (i <= size) {
            std::cout << "*" << " ";
            ++i;
        }
        std::cout << std::endl;
        ++j;
    }
    return 0;
}
