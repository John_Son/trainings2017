#include <iostream>
#include <cmath>
#include <cassert>

double
hypotenuse (const double side1, const double side2) 
{
    assert(side1 > 0 && side2 > 0);
    return std::sqrt(side1 * side1 + side2 * side2);
}

int
main()
{
    double side1;
    std::cout << "Enter first side: ";
    std::cin  >> side1;
    double side2;
    std::cout << "Enter second side: ";
    std::cin >> side2;
    if (side1 <= 0 || side2 <= 0) {
        std::cerr << "Error 1: Invalid number. " << std::endl;
        return 1;
    }

    std::cout << "Hypotenuse is "  << hypotenuse(side1, side2)  << std::endl;
    return 0;
}
