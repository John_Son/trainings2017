#include <iostream>

void getSalaries(int * array, int ARRAY_SIZE);

int
main()
{
    const int ARRAY_SIZE = 9;
    int arrayCount[ARRAY_SIZE] = {0};

    getSalaries(arrayCount, ARRAY_SIZE);

    std::cout << std::endl;
    for (int i = 0; i < ARRAY_SIZE - 1; ++i) {
        const int rangeOfnumber = 100 * (i + 2);
        std::cout << i + 1 << ". $" << rangeOfnumber << " - " << rangeOfnumber + 99 << "\t";
        std::cout << "\t" << arrayCount[i];
        
        std::cout << std::endl;
    }

    std::cout << "9. $1000 and more ";
    std::cout << "\t" << arrayCount[ARRAY_SIZE - 1];

    std::cout << std::endl;
    return 0;
}

void 
getSalaries(int* array, int ARRAY_SIZE)
{
    do
    {
        std::cout << "If you want break Enter -1 \t Enter Week Sale: ";
        int sale;
        std::cin >> sale;

        if (-1 == sale) {
            break;
        }
        if (-1 > sale) {
            std::cerr << "\nError 1: Sale can not be negative." << std::endl;
            break;
        }
                
        const int  allSalary = (sale * 9 / 100) + 200;

        if (1000 < allSalary) {
            ++array[ARRAY_SIZE - 1];
        } else {
            ++array[allSalary / 100 - 2];
        }
    } while (true);
}
